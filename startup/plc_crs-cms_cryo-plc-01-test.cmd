
# @field REQUIRE_plc_crs-cms_cryo-plc-01_VERSION
# @runtime YES

# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_plc_crs-cms_cryo-plc-01_PATH
# @runtime YES

# Load plc interface database
dbLoadRecords("plc_crs-cms_cryo-plc-01-test.db", "MODVERSION=$(REQUIRE_plc_crs-cms_cryo-plc-01_VERSION)")

# Configure autosave
# Number of sequenced backup files to write
save_restoreSet_NumSeqFiles(1)

# Specify directories in which to search for request files
set_requestfile_path("$(REQUIRE_plc_crs-cms_cryo-plc-01_PATH)", "misc")

# Specify where the save files should be
set_savefile_path("$(SAVEFILE_DIR)", "")

# Specify what save files should be restored
set_pass0_restoreFile("plc_crs-cms_cryo-plc-01-test.sav")

# Create monitor set
doAfterIocInit("create_monitor_set('plc_crs-cms_cryo-plc-01-test.req', 1, '')")
