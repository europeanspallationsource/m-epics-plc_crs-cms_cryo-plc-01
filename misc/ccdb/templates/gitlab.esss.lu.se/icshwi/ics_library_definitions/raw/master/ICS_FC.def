###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_FC                                                               ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                                  FC - Flow Controller                                    ##
##                                                                                          ##  
##                                                                                          ##  
############################           Version: 1.3           ################################
# Author:	Miklos Boros 
# Date:		30-10-2019
# Version:  v1.3
# Changes:
# Engineering unit meved to slot property
############################           Version: 1.2           ################################
# Author:	Miklos Boros 
# Date:		28-02-2019
# Version:  v1.2
# Changes:
# 1. Changed smoother ramping
############################           Version: 1.1           ################################
# Author:	Miklos Boros, Marino Vojneski 
# Date:		12-06-2018
# Version:  v1.1
# Changes:
# 1. Modified Alarm Signal section to be compatible with new format.
############################           Version: 1.0           ################################
# Author:	Miklos Boros 
# Date:		08-01-2018
# Version:  v1.0
##############################################################################################



############################
#  STATUS BLOCK
############################ 
define_status_block()


#Operation modes
add_digital("OpMode_Auto",                           PV_DESC="Operation Mode Auto",        PV_ONAM="True",             PV_ZNAM="False")      
add_digital("OpMode_Manual",                         PV_DESC="Operation Mode Manual",      PV_ONAM="True",             PV_ZNAM="False")
add_digital("OpMode_Forced",                         PV_DESC="Operation Mode Forced",      PV_ONAM="True",             PV_ZNAM="False")
add_analog("FCColor","INT",                          PV_DESC="BlockIcon color")


#Flow Controller states
add_analog("ActualFlow","REAL",                      PV_DESC="Actual Flow",                PV_EGU="[PLCF#EGU]")
add_analog("FlowSP","REAL",                          PV_DESC="Flow SetPoint",              PV_EGU="[PLCF#EGU]")
add_analog("FlowMV","REAL",                          PV_DESC="Flow Control AO",            PV_EGU="[PLCF#EGU]")
add_analog("RAWValue", "REAL",                       PV_DESC="RAW integer scaled" )
add_digital("Opening",                               PV_DESC="Valve opening",              PV_ONAM="Opening",          PV_ZNAM="NotMoving")
add_digital("Closing",                               PV_DESC="Valve closing",              PV_ONAM="Closing",          PV_ZNAM="NotMoving")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",                        PV_DESC="Inhibit Manual Mode",        PV_ONAM="InhibitManual",    PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",                         PV_DESC="Inhibit Force Mode",         PV_ONAM="InhibitForce",     PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",                          PV_DESC="Inhibit Locking",            PV_ONAM="InhibitLocking",   PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("GroupInterlock",                        PV_DESC="Group Interlock",            PV_ONAM="True",             PV_ZNAM="False")
add_string("InterlockMsg", 39,                       PV_NAME="InterlockMsg",               PV_DESC="Interlock Message")
add_digital("MoveInterlock",                         PV_DESC="Move Interlock",             PV_ONAM="True",             PV_ZNAM="False")

#for OPI visualization
add_digital("EnableAutoBtn",                         PV_DESC="Enable Auto Button",         PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableManualBtn",                       PV_DESC="Enable Manual Button",       PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableForcedBtn",                       PV_DESC="Enable Force Button",        PV_ONAM="True",             PV_ZNAM="False")

#Block Icon controls
add_digital("EnableBlkCtrl",                         PV_DESC="Enable Block SP Button",     PV_ONAM="True",             PV_ZNAM="False")


#Scaling limits
add_analog("SPScaleMin","REAL",                      PV_DESC="Minimum for SP",             PV_EGU="[PLCF#EGU]")
add_analog("SPScaleMax","REAL",                      PV_DESC="Maximum for SP",             PV_EGU="[PLCF#EGU]")

#Forcing
add_digital("EnableForceValBtn",                     PV_DESC="Enable Force Value Button",  PV_ONAM="True",             PV_ZNAM="False")

#Locking mechanism
add_digital("DevLocked",                             PV_DESC="Device locked",              PV_ONAM="True",             PV_ZNAM="False")
add_analog("Faceplate_LockID","DINT",                PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID","DINT",                PV_DESC="Guest Lock ID")

add_digital("LatchAlarm",                            PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                            PV_DESC="Group Alarm for OPI")

#Alarm signals
add_major_alarm("Position_Discrep","Position Discrepancy",           PV_ZNAM="NominalState")
add_major_alarm("IO_Error","HW IO Error",                            PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error","HW Input Module Error",        PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error","HW Output Module Error",      PV_ZNAM="NominalState")
add_minor_alarm("SPLimitActive","SPLimitActive",                     PV_ZNAM="NominalState")
add_major_alarm("SSTriggered","SSTriggered",                         PV_ZNAM="NominalState")

#Discrepancy
add_analog("DiscrPerc","REAL" ,                      PV_DESC="Discrepancy In Percent",                   PV_EGU="%")
add_time("DiscrTime",                                PV_DESC="Discrepancy Time Interval")

#Feedbacks
add_analog("FB_ForcePosition","REAL" ,               PV_DESC="FB Force Position (AI)",                   PV_EGU="[PLCF#EGU]")
add_analog("FB_Setpoint","REAL" ,                    PV_DESC="FB Setpoint from HMI (SP)",                PV_EGU="[PLCF#EGU]")
add_analog("FB_Manipulated","REAL" ,                 PV_DESC="FB Manipulated Value (AO)",                PV_EGU="[PLCF#EGU]")
add_analog("FB_Step","REAL" ,                        PV_DESC="FB Step value for Open/Close",             PV_EGU="[PLCF#EGU]")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                              PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",                            PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",                             PV_DESC="CMD: Force Mode")

add_digital("Cmd_RampON",                            PV_DESC="Turn Ramping ON")
add_digital("Cmd_RampOFF",                           PV_DESC="Turn Ramping OFF")

add_digital("Cmd_AckAlarm",                          PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceValInp",                       PV_DESC="CMD: Force PLC Input")
add_digital("Cmd_ForceValOut",                       PV_DESC="CMD: Force PLC Output")

add_digital("Cmd_ForceUnlock",                       PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",                           PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",                         PV_DESC="CMD: Unlock Device")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Setpoint and Manipulated value from HMI
add_analog("P_ForcePosition","REAL" ,                PV_DESC="Force position (AI)",                   PV_EGU="[PLCF#EGU]")
add_analog("P_Setpoint","REAL" ,                     PV_DESC="Setpoint from HMI (SP)",                PV_EGU="[PLCF#EGU]")
add_analog("P_Manipulated","REAL" ,                  PV_DESC="Manipulated value (AO)",                PV_EGU="[PLCF#EGU]")

#Step value when pressing Cmd_Open1Step or Cmd_Close1Step
add_analog("P_Step","REAL" ,                         PV_DESC="Step value for open close",             PV_EGU="%")

#Locking mechanism
add_analog("P_Faceplate_LockID","DINT",              PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID","DINT",              PV_DESC="Device ID after Blockicon Open")
