###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                               Standard State Machine Step                                ##
##                                                                                          ##  
##                                                                                          ##  
############################ Version: 1.0             ########################################
# Author:  Miklos Boros 
# Date:    06-05-2020
# Version: v1.0



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Step Messages
add_string("StrMsg1", 39,         PV_NAME="StrMsg1",            PV_DESC="Step Message 1")
add_string("StrMsg2", 39,         PV_NAME="StrMsg2",            PV_DESC="Step Message 2")


#Status BITs
add_digital("STS_WDInactive",         PV_DESC="Watchdog Inactive",               PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_WDActive",           PV_DESC="Watchdog Active",                 PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_WDWarning",          PV_DESC="Watchdog Warning",                PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_F_Active",           PV_DESC="Failure Action Active",           PV_ONAM="True",                       PV_ZNAM="False")

#Initial condition
add_analog("STS_ActStep",             "INT",                                     PV_DESC="Step Actual Step ID")

#State dependent Interlock
add_minor_alarm("StateInterlock",     "StateInterlock Active",                                    PV_ZNAM="True")

#Active BlockIcon Buttons
add_digital("BTN_Act_Ena",            PV_DESC="Activate Button Enable",          PV_ONAM="True",                       PV_ZNAM="False")
add_digital("BTN_Act_Disa",           PV_DESC="Activate Button Disable",         PV_ONAM="True",                       PV_ZNAM="False")


#State alarms
add_digital("GroupAlarm",             PV_DESC="Group Alarm",                     PV_ONAM="True",                       PV_ZNAM="False")
add_major_alarm("Alarm1",            "Alarm1",                                                    PV_ZNAM="True")
add_major_alarm("Alarm2",            "Alarm2",                                                    PV_ZNAM="True")
add_major_alarm("Alarm3",            "Alarm3",                                                    PV_ZNAM="True")
add_major_alarm("Alarm4",            "Alarm4",                                                    PV_ZNAM="True")
add_major_alarm("Alarm5",            "Alarm5",                                                    PV_ZNAM="True")

add_minor_alarm("Warning1",          "Warning1",                                                  PV_ZNAM="True")
add_minor_alarm("Warning2",          "Warning2",                                                  PV_ZNAM="True")
add_minor_alarm("Warning3",          "Warning3",                                                  PV_ZNAM="True")
add_minor_alarm("Warning4",          "Warning4",                                                  PV_ZNAM="True")
add_minor_alarm("Warning5",          "Warning5",                                                  PV_ZNAM="True")


#Flag bits
add_digital("Flag1",                  PV_DESC="Flag1",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag2",                  PV_DESC="Flag2",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag3",                  PV_DESC="Flag3",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag4",                  PV_DESC="Flag4",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag5",                  PV_DESC="Flag5",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag6",                  PV_DESC="Flag6",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag7",                  PV_DESC="Flag7",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag8",                  PV_DESC="Flag8",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag9",                  PV_DESC="Flag9",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag10",                 PV_DESC="Flag10",     PV_ONAM="True",                       PV_ZNAM="False")


#Parameter Feedback
add_analog("FB_S1",            "REAL",                           PV_DESC="Feedback Parameter1")
add_analog("FB_S2",            "REAL",                           PV_DESC="Feedback Parameter2")
add_analog("FB_S3",            "REAL",                           PV_DESC="Feedback Parameter3")
add_analog("FB_S4",            "REAL",                           PV_DESC="Feedback Parameter4")
add_analog("FB_S5",            "REAL",                           PV_DESC="Feedback Parameter5")
add_analog("FB_S6",            "REAL",                           PV_DESC="Feedback Parameter6")
add_analog("FB_S7",            "REAL",                           PV_DESC="Feedback Parameter7")
add_analog("FB_S8",            "REAL",                           PV_DESC="Feedback Parameter8")
add_analog("FB_S9",            "REAL",                           PV_DESC="Feedback Parameter9")
add_analog("FB_S10",           "REAL",                           PV_DESC="Feedback Parameter10")
add_analog("FB_S11",           "REAL",                           PV_DESC="Feedback Parameter11")
add_analog("FB_S12",           "REAL",                           PV_DESC="Feedback Parameter12")
add_analog("FB_S13",           "REAL",                           PV_DESC="Feedback Parameter13")
add_analog("FB_S14",           "REAL",                           PV_DESC="Feedback Parameter14")
add_analog("FB_S15",           "REAL",                           PV_DESC="Feedback Parameter15")
add_analog("FB_S16",           "REAL",                           PV_DESC="Feedback Parameter16")
add_analog("FB_S17",           "REAL",                           PV_DESC="Feedback Parameter17")
add_analog("FB_S18",           "REAL",                           PV_DESC="Feedback Parameter18")
add_analog("FB_S19",           "REAL",                           PV_DESC="Feedback Parameter19")
add_analog("FB_S20",           "REAL",                           PV_DESC="Feedback Parameter20")

############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("CMD_Activate",           PV_DESC="CMD_Activate",        PV_ONAM="True",              PV_ZNAM="False")
add_digital("CMD_DeActivate",         PV_DESC="CMD_DeActivate",      PV_ONAM="True",              PV_ZNAM="False")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

# State Machine Operator ON/OFF Bits
add_digital("OP_ONOFF_1",           PV_DESC="Operator ONOFF 1",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_2",           PV_DESC="Operator ONOFF 2",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_3",           PV_DESC="Operator ONOFF 3",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_4",           PV_DESC="Operator ONOFF 4",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_5",           PV_DESC="Operator ONOFF 5",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_6",           PV_DESC="Operator ONOFF 6",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_7",           PV_DESC="Operator ONOFF 7",       PV_ONAM="True",                PV_ZNAM="False")
add_digital("OP_ONOFF_8",           PV_DESC="Operator ONOFF 8",       PV_ONAM="True",                PV_ZNAM="False")

add_analog("S1",            "REAL",                           PV_DESC="Parameter1")
add_analog("S2",            "REAL",                           PV_DESC="Parameter2")
add_analog("S3",            "REAL",                           PV_DESC="Parameter3")
add_analog("S4",            "REAL",                           PV_DESC="Parameter4")
add_analog("S5",            "REAL",                           PV_DESC="Parameter5")
add_analog("S6",            "REAL",                           PV_DESC="Parameter6")
add_analog("S7",            "REAL",                           PV_DESC="Parameter7")
add_analog("S8",            "REAL",                           PV_DESC="Parameter8")
add_analog("S9",            "REAL",                           PV_DESC="Parameter9")
add_analog("S10",           "REAL",                           PV_DESC="Parameter10")
add_analog("S11",           "REAL",                           PV_DESC="Parameter11")
add_analog("S12",           "REAL",                           PV_DESC="Parameter12")
add_analog("S13",           "REAL",                           PV_DESC="Parameter13")
add_analog("S14",           "REAL",                           PV_DESC="Parameter14")
add_analog("S15",           "REAL",                           PV_DESC="Parameter15")
add_analog("S16",           "REAL",                           PV_DESC="Parameter16")
add_analog("S17",           "REAL",                           PV_DESC="Parameter17")
add_analog("S18",           "REAL",                           PV_DESC="Parameter18")
add_analog("S19",           "REAL",                           PV_DESC="Parameter19")
add_analog("S20",           "REAL",                           PV_DESC="Parameter20")

